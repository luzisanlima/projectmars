using Xerpa.ProjectMars.Core.Engine;
using Xerpa.ProjectMars.Core.Entities;
using Xunit;

namespace Xerpa.ProjectMars.Tests
{
    public class ProbeTests
    {
        [Theory]
        [InlineData(2, 2, Direction.North)]
        public void LandOnValidPosition(int width, int height, Direction direction)
        {
            var area    = new Area();
            var probe   = new Probe();

            area.SetSize(10, 10);

            probe.Land(area, new Position(width, height), direction);

            Assert.Equal(probe.Position.X, width);
            Assert.Equal(probe.Position.Y, height);
        }

        [Theory]
        [InlineData(2, 2, Direction.North)]
        public void LandOnInvalidPosition(int width, int height, Direction direction)
        {
            var area    = new Area();
            var probe   = new Probe();

            area.SetSize(1, 1);
            probe.Land(area, new Position(width, height), direction);

            Assert.Null(probe.Position);
        }

        [Theory]
        [InlineData(Direction.North,    Direction.West)]
        [InlineData(Direction.West,     Direction.South)]
        [InlineData(Direction.South,    Direction.East)]
        [InlineData(Direction.East,     Direction.North)]
        public void TurnLeft(Direction initial, Direction expected)
        {
            var marte = new Probe{
                Direction = initial
            };

            marte.LeftRotation();

            Assert.Equal(expected, marte.Direction);
        }

        [Theory]
        [InlineData(Direction.North,    Direction.East)]
        [InlineData(Direction.East,     Direction.South)]
        [InlineData(Direction.South,    Direction.West)]
        [InlineData(Direction.West,     Direction.North)]
        public void TurnRight(Direction initial, Direction expected)
        {
            var marte = new Probe();
            marte.Direction = initial;

            marte.RightRotation();

            Assert.Equal(expected, marte.Direction);
        }

        [Theory]
        [InlineData(Direction.North,    2, 3)]
        [InlineData(Direction.East,     3, 2)]
        [InlineData(Direction.South,    2, 1)]
        [InlineData(Direction.West,     1, 2)]
        public void MoveForward(Direction direction,int expectedXAxis, int expectedYAxis)
        {
            var area = new Area();
            area.SetSize(3, 3);

            var probe = new Probe
            {
                Position = new Position(2, 2),
                Direction = direction,
                Area = area
            };

            probe.MoveForward();

            Assert.Equal(probe.Position.X, expectedXAxis);
            Assert.Equal(probe.Position.Y, expectedYAxis);
        }
    }
}
