using Xerpa.ProjectMars.Core.Entities;
using Xunit;

namespace Xerpa.ProjectMars.Tests
{
    public class AreaTests
    {
        [Theory]
        [InlineData(5, 5)]
        public void SetValidAreaSize(int height, int width)
        {
            Area area = new Area(); 
            area.SetSize(height, width);
            
            Assert.Equal(area.Size.Height, height);
            Assert.Equal(area.Size.Width, width);
        }

        [Theory]
        [InlineData(-1, 4)]
        [InlineData(4, -1)]
        public void SetInvalidAreaSize(int height, int width)
        {
            Area area = new Area();
            area.SetSize(height, width);
            Assert.Null(area.Size);
        }
    }
}