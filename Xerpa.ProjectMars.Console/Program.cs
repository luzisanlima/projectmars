using Xerpa.ProjectMars.Core.Entities;
using Xerpa.ProjectMars.Core.Engine;

namespace Xerpa.ProjectMars.Console
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var area    = new Area();
            var probe   = new Probe();

            while (true)
            {
                var commandInput = System.Console.ReadLine();

                if (!string.IsNullOrEmpty(commandInput))
                {
                    var controller = new Controller(commandInput);
                    switch (controller.CommandType)
                    {
                        case CommandType.CreateArea:
                            area.SetSize(controller.Size.Height, controller.Size.Width);
                            break;

                        case CommandType.Land:
                            probe.Land(area, controller.Position, controller.Direction);
                            System.Console.WriteLine(probe.GetLocation());
                            break;

                        case CommandType.Move:
                            probe.Move(controller.Movements);
                            System.Console.WriteLine(probe.GetLocation());
                            break;
                    }
                }
            }
        }
    }
}
