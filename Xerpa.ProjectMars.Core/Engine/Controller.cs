using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Xerpa.ProjectMars.Core.Engine
{
    public class Controller
    {
        public Size Size                { get; set; }
        public Position Position        { get; set; }
        public List<Movement> Movements { get; set; }
        public Direction Direction      { get; set; }
        public CommandType CommandType  { get; set; }


        public Controller(string inputCommand)
        {
            CommandType = commandParser(inputCommand);

            switch (CommandType)
            {
                case CommandType.CreateArea:

                    var sizes = inputCommand.Split(' ').Select(x => int.Parse(x)).ToArray();
                    Size = new Size(sizes[0], sizes[1]);
                    Console.WriteLine($"�rea definida: {Size.Height} - {Size.Width}");

                    break;
                case CommandType.Land:

                    var positions = inputCommand.Split(' ').ToArray();
                    Position = new Position(int.Parse(positions[0]), int.Parse(positions[1]));
                    Direction = directionParser(positions[2]);
                    Console.WriteLine($"Pouso em: X:{Position.X} - Y: {Position.Y} - Dire��o: {Direction}");

                    break;
                case CommandType.Move:
                    Movements = movementParser(inputCommand);
                    Console.WriteLine($"Movimentos: {inputCommand}");
                    break;
                default:
                    Console.WriteLine("Comando n�o identificado");
                    break;
            }


            List<Movement> movementParser(string inputDirection)
            {
                List<Movement> movements = new List<Movement>();

                foreach (var move in inputCommand)
                {
                    switch (move)
                    {
                        case 'M':
                            movements.Add(Movement.Forward);
                            break;
                        case 'L':
                            movements.Add(Movement.Left);
                            break;
                        case 'R':
                            movements.Add(Movement.Right);
                            break;
                        default:
                            break;
                    }
                }
                return movements;
            }

            Direction directionParser(string inputDirection)
            {
                switch (Convert.ToChar(inputDirection))
                {
                    case 'N':
                        return Direction.North;
                    case 'E':
                        return Direction.East;
                    case 'S':
                        return Direction.South;
                    case 'W':
                        return Direction.West;
                    default:
                        return Direction.None;
                }
            }

            CommandType commandParser(string inputCommand)
            {
                if (Regex.Match(inputCommand, "^([0-9]+ [0-9]+)$").Success)
                    return CommandType.CreateArea;

                if (Regex.Match(inputCommand, "^([0-9]+ [0-9]+ (N|S|E|W))$").Success)
                    return CommandType.Land;

                if (Regex.Match(inputCommand, @"^((L|R|M)+)$").Success)
                    return CommandType.Move;

                return CommandType.None;
            }
        }
    }
}
