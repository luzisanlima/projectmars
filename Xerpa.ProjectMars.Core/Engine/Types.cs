namespace Xerpa.ProjectMars.Core.Engine
{
    public enum Movement
    {
        Left,
        Right,
        Forward
    }

    public enum Direction
    {
        North,
        East,
        South,
        West,
        None
    }

    public enum CommandType
    {
        CreateArea,
        Land,
        Move,
        None
    }
}
