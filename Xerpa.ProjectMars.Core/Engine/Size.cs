namespace Xerpa.ProjectMars.Core.Engine
{
    public class Size
    {
        public int Height   { get; set; }
        public int Width    { get; set; }

        public Size(int height, int width)
        {
            Height  = height;
            Width   = width;
        }
    }
}
