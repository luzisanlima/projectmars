using Xerpa.ProjectMars.Core.Engine;
using System.Collections.Generic;
using System;

namespace Xerpa.ProjectMars.Core.Entities
{
    public class Probe
    {
        public Area Area { get; set; }
        public Position Position { get; set; }
        public Direction Direction { get; set; }

        public void Move(List<Movement> movements)
        {
            foreach (var move in movements)
            {
                switch (move)
                {
                    case Movement.Left:
                        LeftRotation();
                        break;
                    case Movement.Right:
                        RightRotation();
                        break;
                    case Movement.Forward:
                        MoveForward();
                        break;
                    default:
                        break;
                }
            }
        }

        public void MoveForward()
        {
            var position = new Position(Position.X, Position.Y);
            switch (Direction)
            {
                case Direction.North:
                    position.Y++;
                    break;
                case Direction.East:
                    position.X++;
                    break;
                case Direction.South:
                    position.Y--;
                    break;
                case Direction.West:
                    position.X--;
                    break;
                default:
                    break;
            }

            if (isValidAreaPosition(Area, position))
            {
                Position = position;
                return;
            }

            Console.WriteLine("Movimento invalido");
        }

        public void Land(Area area, Position position, Direction direction)
        {
            if (isValidAreaPosition(area, position))
            {
                Position = position;
                Direction = direction;
                Area = area;

                return;
            }

            Console.WriteLine("Marte - Invalid Land Location!");
        }

        public string GetLocation()
        {
            return $"{Position.X} {Position.Y} {Direction.ToString().Substring(0, 1)}";
        }

        public void RightRotation()
        {
            Direction = Direction == Direction.West ? Direction.North : Direction += 1;
        }

        public void LeftRotation()
        {
            Direction = Direction == Direction.North ? Direction.West : Direction -= 1;
        }

        private bool isValidAreaPosition(Area area, Position position)
        {
            return (position.X <= area.Size.Width) && (position.Y <= area.Size.Height);
        }
    }
}
