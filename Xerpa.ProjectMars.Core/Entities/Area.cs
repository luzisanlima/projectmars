using System;
using Xerpa.ProjectMars.Core.Engine;

namespace Xerpa.ProjectMars.Core.Entities
{
    public class Area
    {
        public Size Size { get; set; }

        public void SetSize(int height, int width)
        {
            if (height < 1 || width < 1) 
                Console.WriteLine("Invalid Size.");
            else 
                Size = new Size(height, width);
        }
    }
}
